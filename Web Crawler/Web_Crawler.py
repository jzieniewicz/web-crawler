import urllib.request
 
d = {}
dict={}
bylo=[]
 
print('Enter url:')
base = input()
 
def site_map(url):    
    global dict
    path = url  
    req=urllib.request.urlopen(path)
    data=req.read()
    html=data.decode("UTF-8")
   # print(html)
    title=html[html.find('<title>')+len('<title>'):html.find('</title>',html.find('<title>'))]  #finding title between start and end title tags
    #print(title)
    links=[]
    start= -1
    while True:    
        start=html.find('<a href="',start + 1)  #finding all the links until find function doesn't return -1
        if start == -1:
            break            
        found=html[start+len('<a href="'):html.find('">',start)] #link is a string between '<a href' and '>'
        if not found.startswith("https://"): #if it doesn't start with https:// or http:// it is subpage and it have to use a base variable as a begin
            if not found.startswith("http://"):          
                    found=base+found        
            links.append(found)
           
            copy=[] #to remove repeating links
            for link in links:
                if link not in copy:
                    copy.append(link)
            links=copy
   
    #if "http://127.0.0.1:8000/site.html" in links:
     #   links.remove("http://127.0.0.1:8000/site.html")
           
    d={title:links}
    dict[url] = d
    #print(links)
    for link in links:    
        if link not in bylo:
            bylo.append(link)      
            site_map(link)          
    return dict
 
   
 
#print(site_map(base))
 
print('{')
for x in site_map(base):
    print ('\t',x,'{')
    for y in site_map(base)[x]:
        print ('\t\t',y,':',site_map(base)[x][y])
        print('\t}')
print('}')
